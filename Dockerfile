FROM node:7
MAINTAINER PubSweet Team <richard@coko.foundation>

# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
RUN groupadd --system pubsweet && useradd --system --create-home --gid pubsweet pubsweet
ENV HOME "/home/pubsweet"

ENV ARGUMENTS=()

RUN apt-get update && apt-get install -y apt-utils && \
  echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections && \
  apt-get install -y \
  xvfb \
  x11-xkb-utils \
  xfonts-100dpi \
  xfonts-75dpi \
  xfonts-scalable \
  xfonts-cyrillic \
  x11-apps \
  clang \
  libssl-dev \
  wget \
  nano \
  htop \
  tmux \
  libdbus-1-dev \
  libgtk2.0-dev \
  libnotify-dev \
  libgnome-keyring-dev \
  libgconf2-dev \
  libasound2-dev \
  libcap-dev \
  libcups2-dev \
  libxtst-dev \
  libxss1 \
  libnss3-dev \
  gcc-multilib \
  g++-multilib && \
  rm -rf /var/lib/apt/lists/* && \
  find /usr/share/doc -depth -type f ! -name copyright | xargs rm || true && \
  find /usr/share/doc -empty | xargs rmdir || true && \
  rm -rf /usr/share/man/* /usr/share/groff/* /usr/share/info/* && \
  rm -rf /usr/share/lintian/* /usr/share/linda/* /var/cache/man/*

WORKDIR ${HOME}
RUN yarn config set yarn-offline-mirror ./npm-packages-offline-cache && \
  yarn config set ignore-optional true && \
  yarn config set no-progress true && \
  yarn config set prefer-offline true && \
  yarn config set yarn-offline-mirror-pruning true

RUN git config --global user.email "test@testing.com" && \
    git config --global user.name "Fakey McFakerson"

RUN cd && \
    git clone https://gitlab.coko.foundation/pubsweet/pubsweet-server.git && \
    cd pubsweet-server && \
    yarn && yarn clean
RUN cd && \
    git clone https://gitlab.coko.foundatxion/pubsweet/pubsweet-client.git && \
    cd pubsweet-client && \
    yarn && yarn clean
RUN cd && \
    git clone https://gitlab.coko.foundation/pubsweet/pubsweet-cli.git && \
    cd pubsweet-cli && yarn && yarn clean  
RUN cd && \
    ./pubsweet-cli/bin/pubsweet.js new exampleapp && \
    cd exampleapp && yarn clean && cd

VOLUME ${HOME}

